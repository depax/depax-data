/**
 * The error that is raised when trying to create a reference from an invalid
 * field type.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */
export default class EInvalidReferenceType extends Error {
    readonly field: string;
    readonly type: string;
    /**
     * Create the error, and define the error values.
     *
     * @param field The name of the field causing the error.
     * @param type The value type of the field causing the error.
     */
    constructor(field: string, type: string);
}
