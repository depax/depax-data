/**
 * Provides the Data class allowing object manipulation.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */
import * as EventEmitter from "eventemitter3";
export interface IData {
    [name: string]: any;
}
export default class Data extends EventEmitter {
    protected data: IData;
    protected xpathSeperator: string;
    /** Determines if the data has been modified. */
    protected modified: boolean;
    /**
     * Construct the data access object, and provide any additional details.
     *
     * @param data The data object to manipulate, if null then one is created.
     * @param xpathSeperator The seperator to use to seperate each xpath element.
     */
    constructor(data?: IData, xpathSeperator?: string);
    /** Returns the defined seperator character. */
    readonly seperator: string;
    /** Returns the raw data object. */
    readonly getData: any;
    /** Determines if the data has been modified. */
    readonly isModified: boolean;
    /**
     * Resets the internal data references to the provided data object.
     *
     * @param data The data object to reset the internal reference.
     *
     * @returns Returns self.
     *
     * @emits reset Emitted with this.
     */
    reset(data?: IData): Data;
    /**
     * Returns the data as a JSON.
     *
     * @param pretty Output the JSON in a pretty view.
     *
     * @returns Returns the JSON output.
     */
    toJson(pretty?: boolean): string;
    /**
     * Returns the data as a YAML.
     *
     * @returns Returns the YAML output.
     */
    toYaml(): string;
    /**
     * Imports and sets the data from a JSON string.
     *
     * @param data The JSON string to parse into the data object.
     *
     * @returns Returns self.
     */
    fromJson(data: string): Data;
    /**
     * Imports and sets the data from a YAML string.
     *
     * @param data The YAML string to parse into the data object.
     *
     * @returns Returns self.
     */
    fromYaml(data: string): Data;
    /**
     * Determines if the provided property exists.
     *
     * @param name The name of the property to check.
     *
     * @returns Returns true or false.
     */
    has(name: string): boolean;
    /**
     * Get the value of the property, or the default value.
     *
     * @param name The name of the property to get.
     * @param def The default value to return if the property doesnt exist.
     *
     * @returns Returns the property value, otherwise the def value.
     */
    get(name: string, def?: any): any;
    /**
     * Set the value of a property.
     *
     * @param name The name of the property to set.
     * @param value The value to assign, if the value is null then the field is deleted.
     *
     * @returns Returns self.
     *
     * @emits update Emitted with this, name and value.
     */
    set(name: string, value: any): Data;
    /**
     * Deletes the value of a property.
     *
     * @param name The name of the property to delete.
     *
     * @returns Returns self.
     *
     * @emits update Emitted with this, and name.
     */
    del(name: string): Data;
    /**
     * Generate a reference to the specified xpath, this only works if the value
     * is an object. If the value doesnt exist, one is created as an empty
     * object.
     *
     * @param name The name of the property to reference.
     * @param def The default value of the object property.
     *
     * @returns The resulting reference.
     *
     * @throws {EInvalidReferenceType} Thrown if the value is anything but an object.
     */
    ref(name: string, def?: any): DataReference;
    /**
     * Helper function to convert a property name to an XPath used through eval
     * to get or set a config value.
     *
     * @param name The name of the property.
     *
     * @returns The name converted to an eval xpath.
     */
    protected nameToXPath(name: string): string;
}
export declare class DataReference extends Data {
    protected parent: Data;
    protected xpath: string;
    /**
     * Construct the data reference object.
     *
     * @param parent A parent Data object if this is a reference of an item from the parent.
     * @param xpath The xpath of the reference within the parent.
     * @param data The data object to manipulate, if null then one is created.
     * @param seperator The seperator to use to seperate each xpath element.
     */
    constructor(parent: Data, xpath: string, data: any, seperator: string);
    /** Get the parent reference. */
    readonly getParent: Data;
    /** Get the root data element. */
    readonly getRoot: Data;
    /** @inheritdoc */
    reset(data?: any): Data;
    /** @inheritdoc */
    set(name: string, value: any): Data;
    /** @inheritdoc */
    del(name: string): Data;
}
