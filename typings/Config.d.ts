/**
 * Provides the Data Config class.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */
import Data from "./Data";
declare const sFilename: unique symbol;
export default class Config extends Data {
    /** @todo */
    private [sFilename];
    /**
     * Construct the data access object, and provide any additional details.
     *
     * @param filename The filename of the config.
     * @param data The data object to manipulate, if null then one is created.
     * @param seperator The seperator to use to seperate each xpath element.
     */
    constructor(filename: string, data?: any, seperator?: string);
    /** Get and set the config filename. */
    filename: string;
    /**
     * Loads the config file.
     *
     * @emits loaded Emitted after the data has been loaded from a file.
     *
     * @throws {EMissingFilename} Thrown if the filename has not been provided or set.
     */
    load(): Promise<void>;
    /**
     * Saves the config file.
     *
     * @emits saved Emitted after the data has been saved to a file.
     *
     * @throws {EMissingFilename} Thrown if the filename has not been provided or set.
     */
    save(): Promise<void>;
}
export {};
