/**
 * Provides the route file of the module exposing all the relevant classes.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */
import Config from "./Config";
import Data, { DataReference } from "./Data";
import EInvalidReferenceType from "./Errors/EInvalidReferenceType";
export default Data;
export { Config, DataReference, EInvalidReferenceType, };
