/**
 * Provides the step definitions for the feature tests.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { assert } from "chai";
import { After, Given, Then, When } from "cucumber";
import { readFile, unlink, writeFile } from "fs";
import { tmpdir } from "os";
import { join } from "path";
import { promisify } from "util";
import * as yaml from "yaml";
import Data, { Config, EInvalidReferenceType } from "../../src";

const pReadFile = promisify(readFile);
const pWriteFile = promisify(writeFile);
const pUnlink = promisify(unlink);

let internalData: Data | Config;
let internalReferences: any = {};

After(async () => {
    internalData = null;
    internalReferences = {};
});

//#region ----< Define the *Given* steps >------------------------------------------------------------------------------
Given(/^I have an empty data instance$/, async (): Promise<void> => {
    internalData = new Data();
});

Given(/^I have a data instance with$/, async (
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    internalData = new Data(data);
});

Given(/^I have an empty data instance using the seperator (.*)$/, async (
    seperator: string,
): Promise<void> => {
    internalData = new Data({}, seperator);
});

Given(/^I have an empty config instance called "(.*)"$/, async (
    name: string,
): Promise<void> => {
    internalData = new Config(join(tmpdir(), name));
});

Given(/^I have a config instance called "(.*)" with$/, async (
    name: string,
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    internalData = new Config(join(tmpdir(), name), data);
});

Given(/^I create a (json|yaml) file called "(.*)"$/, async (
    type: string,
    name: string,
    data: any,
): Promise<void> => {
    if (type === "yaml") {
        data = yaml.stringify(JSON.parse(data));
    }
    await pWriteFile(join(tmpdir(), name), data);
});

Given(/^The filename "(.*)" doesnt exist$/, async (
    name: string,
): Promise<void> => {
    try {
        await pUnlink(join(tmpdir(), name));
    } catch (err) {
        // Do nothing.
    }
});
//#endregion

//#region ----< Define the *When* steps >-------------------------------------------------------------------------------
When(/^I set the (\S*) property "(.*)" to "(.*)"$/, async (
    ref: any,
    property: string,
    value: string,
): Promise<void> => {
    try {
        value = JSON.parse(value);
    } catch (err) {
        // Do nothing.
    }

    if (ref === "data") {
        internalData.set(property, value);
    } else {
        internalReferences[ref].set(property, value);
    }
});

When(/^I have reference (\S*) from (\S*) and property "(.*)"$/, async (
    ref: any,
    from: string,
    property: string,
): Promise<void> => {
    if (from === "data") {
        internalReferences[ref] = internalData.ref(property);
    } else {
        internalReferences[ref] = internalReferences[from].ref(property);
    }
});

When(/^I have reference (\S*) from (\S*) and property "(.*)" with$/, async (
    ref: any,
    from: string,
    property: string,
    def: string,
): Promise<void> => {
    def = JSON.parse(def);
    if (from === "data") {
        internalReferences[ref] = internalData.ref(property, def);
    } else {
        internalReferences[ref] = internalReferences[from].ref(property, def);
    }
});

When(/^I delete the (\S*) property "(.*)"$/, async (
    ref: string,
    property: string,
): Promise<void> => {
    ref === "data" ? internalData.del(property) : internalReferences[ref].del(property);
});

When(/^I reset the data$/, async (): Promise<void> => {
    internalData.reset();
});

When(/^I reset the data for (\S*)$/, async (
    ref: string,
): Promise<void> => {
    internalReferences[ref].reset();
});

When(/^I reset the data to$/, async (
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    internalData.reset(data);
});

When(/^I reset the data for (\S*) to$/, async (
    ref: string,
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    internalReferences[ref].reset(data);
});

When(/^I load the config file$/,  async (): Promise<void> => {
    await (internalData as Config).load();
});

When(/^I save the config file$/, async (): Promise<void> => {
    await (internalData as Config).save();
});
//#endregion

//#region ----< Define the *Then* steps >-------------------------------------------------------------------------------
Then(/^I should have the seperator (.*)$/, async (
    seperator: string,
): Promise<void> => {
    assert.propertyVal(internalData, "seperator", seperator);
});

Then(/^The (.*) property "(.*)" (should|shouldnt) exist$/, async (
    ref: string,
    property: string,
    should: string,
): Promise<void> => {
    assert[should === "should" ? "isTrue" : "isFalse"](
        ref === "data" ? internalData.has(property) : internalReferences[ref].has(property),
    );
});

Then(/^The (\S*) property "(\S*)" (|as "(.*)" )(should|shouldnt) be "(.*)"$/, async (
    ref: string,
    property: string,
    as: string,
    should: string,
    value: string,
): Promise<void> => {
    const output = ref === "data" ? internalData.get(property, as) : internalReferences[ref].get(property, as);
    try {
        value = JSON.parse(value);
        assert[should === "should" ? "deepEqual" : "notDeepEqual"](output, value);
    } catch (err) {
        // Do nothing.
        assert[should === "should" ? "equal" : "notEqual"](output, value);
    }
});

Then(/^The (.*) property "(.*)" (should|shouldnt) be undefined$/, async (
    ref: string,
    property: string,
    should: string,
): Promise<void> => {
    assert[should === "should" ? "equal" : "notEqual"](
        ref === "data" ? internalData.get(property) : internalReferences[ref].get(property),
        undefined,
    );
});

Then(/^The modified flag is (true|false)$/, async (
    bool: string,
): Promise<void> => {
    assert[bool === "true" ? "isTrue" : "isFalse"](internalData.isModified);
});

Then(/^The raw data (should|shouldnt) be$/, async (
    bool: string,
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    assert[bool === "should" ? "deepEqual" : "notDeepEqual"](internalData.getData, data);
});

Then(/^The raw data for (\S*) (should|shouldnt) be$/, async (
    ref: string,
    bool: string,
    data: any,
): Promise<void> => {
    data = JSON.parse(data);
    assert[bool === "should" ? "deepEqual" : "notDeepEqual"](internalReferences[ref].getData, data);
});

Then(/^The (pretty|raw) json export (should|shouldnt) be$/, async (
    pretty: string,
    bool: string,
    data: any,
): Promise<void> => {
    if (pretty === "pretty") {
        assert[bool === "should" ? "equal" : "notEqual"](internalData.toJson(), data);
    } else {
        assert[bool === "should" ? "equal" : "notEqual"](internalData.toJson(false), data);
    }
});

Then(/^I import the json$/, async (
    data: any,
): Promise<void> => {
    internalData.fromJson(data);
});

Then(/^I should get an error when creating a reference from (\S*) and property "(.*)"$/, async (
    ref: string,
    property: string,
): Promise<void> => {
    try {
        (ref === "data" ? internalData : internalReferences[ref]).ref(property);
        assert.isTrue(false);
    } catch (err) {
        assert.instanceOf(err, EInvalidReferenceType);
    }
});

Then(/^Reference (\S*) should have (\S*) as a parent$/, async (
    ref: string,
    parent: string,
): Promise<void> => {
    assert.equal(internalReferences[ref].getParent, parent === "data" ?
        internalData :
        internalReferences[parent]);
});

Then(/^Reference (\S*) should have (\S*) as root$/, async (
    ref: string,
    root: string,
): Promise<void> => {
    assert.equal(internalReferences[ref].getRoot, root === "data" ? internalData : internalReferences[root]);
});

Then(/^The config filename is "(.*)"$/, async (
    name: string,
): Promise<void> => {
    assert.equal((internalData as Config).filename, join(tmpdir(), name));
});

Then(/^I set the config filename to "(.*)"$/, async (
    name: string,
): Promise<void> => {
    (internalData as Config).filename = join(tmpdir(), name);
});

Then(/^The filename "(.*)" contains$/, async (
    name: string,
    data: string,
): Promise<void> => {
    const contents = await pReadFile(join(tmpdir(), name));
    assert.equal(contents.toString(), data);
});
