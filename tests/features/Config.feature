@data @config
Feature: Config
  Scenario: Should be able load a JSON config file
    Given I create a json file called "data-config-1.json"
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
      And I have an empty config instance called "data-config-1.json"
     When I load the config file
     Then The config filename is "data-config-1.json"
      And The raw data should be
      """
      {"hello":{"world":{"foo":"bar"}}}
      """

  Scenario: Should be able load a YAML config file
    Given I create a yaml file called "data-config-1.yaml"
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
      And I have an empty config instance called "data-config-1.yaml"
     When I load the config file
     Then The config filename is "data-config-1.yaml"
      And The raw data should be
      """
      {"hello":{"world":{"foo":"bar"}}}
      """

  Scenario: Should be able to change the filename
    Given I have an empty config instance called "data-config-1.json"
     When The config filename is "data-config-1.json"
     Then I set the config filename to "data-config-2.json"
      And The config filename is "data-config-2.json"

  Scenario: Should be able to save the JSON config file
    Given The filename "data-config-1.json" doesnt exist
      And I have a config instance called "data-config-1.json" with
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
     When I save the config file
     Then The filename "data-config-1.json" contains
      """
      {
          "hello": {
              "world": {
                  "foo": "bar"
              }
          }
      }
      """

  Scenario: Should be able to save the YAML config file
    Given The filename "data-config-1.yaml" doesnt exist
      And I have a config instance called "data-config-1.yaml" with
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
     When I save the config file
     Then The filename "data-config-1.yaml" contains
      """
      hello:
        world:
          foo: bar

      """
