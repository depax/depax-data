@data
Feature: Data
  Scenario: Should be able set and get a property
    Given I have an empty data instance
     When I set the data property "hello.world" to "foo bar"
     Then The data property "hello.world" should exist
      And The data property "hello.world" should be "foo bar"

  Scenario: Should be able set an object property
    Given I have an empty data instance
     When I set the data property "hello" to "{"world":"foo bar"}"
     Then The data property "hello.world" should exist
      And The data property "hello" should exist
      And The data property "hello.world" should be "foo bar"
      And The data property "hello" should be "{"world":"foo bar"}"

  Scenario: Should be able set a deep object property
    Given I have an empty data instance
     When I set the data property "hello" to "{"world":{"foo":"bar"}}"
     Then The data property "hello.world.foo" should exist
      And The data property "hello.world" should exist
      And The data property "hello" should exist
      And The data property "hello.world.foo" should be "bar"
      And The data property "hello.world" should be "{"foo":"bar"}"
      And The data property "hello" should be "{"world":{"foo":"bar"}}"
      And I set the data property "hello.world.foo" to "bingo"
      And The data property "hello.world.foo" should be "bingo"
      And The data property "hello.world" should be "{"foo":"bingo"}"
      And The data property "hello" should be "{"world":{"foo":"bingo"}}"

  Scenario: Should be able get an existing property
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
     Then The data property "hello.world" should exist
      And The data property "hello.world" should be "foo bar"
      And The data property "hello" should be "{"world":"foo bar"}"

  Scenario: Should be able get an unknown property with a default value
    Given I have an empty data instance
     Then The data property "hello.world" should be undefined
      And The data property "hello.world" as "foo bar" should be "foo bar"
      And I set the data property "hello.world" to "boo"
      And The data property "hello.world" as "foo bar" should be "boo"

  Scenario: Should be able delete a property
    Given I have an empty data instance
     When I set the data property "hello.world" to "foo bar"
     Then The data property "hello.world" should exist
      And I delete the data property "hello.world"
      And The data property "hello.world" shouldnt exist

  Scenario: Should change the modified flag when setting a property
    Given I have an empty data instance
      And The modified flag is false
     When I set the data property "hello.world" to "foo bar"
     Then The modified flag is true

  Scenario: Should change the modified flag when updating a property
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
      And The modified flag is false
     When I set the data property "hello.world" to "knock knock"
     Then The modified flag is true

  Scenario: Should change the modified flag when deleting a property
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
      And The modified flag is false
     When I delete the data property "hello.world"
     Then The modified flag is true

  Scenario: Should be able to reset the data
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
      And The modified flag is false
     When I reset the data to
      """
      {"foo":{"bar":"hello world"}}
      """
     Then The modified flag is true
      And The data property "hello" shouldnt exist
      And The data property "foo" should exist
      And I reset the data
      And The data property "hello" shouldnt exist
      And The data property "foo" shouldnt exist

  Scenario: Should be able to get the raw data object
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
     When The raw data should be
      """
      {"hello":{"world":"foo bar"}}
      """
     Then I set the data property "hello.world" to "boo"
      And The raw data should be
      """
      {"hello":{"world":"boo"}}
      """

  Scenario: Should export the data to json
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
     Then The raw json export should be
      """
      {"hello":{"world":"foo bar"}}
      """
     And The pretty json export should be
      """
      {
          "hello": {
              "world": "foo bar"
          }
      }
      """

  Scenario: Should import the data from json
    Given I have an empty data instance
     When I import the json
      """
      {"hello":{"world":"foo bar"}}
      """
     Then The data property "hello" should exist
      And The data property "hello" should be "{"world":"foo bar"}"
