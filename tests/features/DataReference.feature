@data @data-reference
Feature: Data Reference
  Scenario: Should be able to create a reference from the original data object
    Given I have a data instance with
      """
      {"hello":{"world":"foo bar"}}
      """
     When I have reference p1 from data and property "hello"
     Then I set the p1 property "world" to "boo"
      And The data property "hello.world" should be "boo"
      And The p1 property "world" should be "boo"
      And The raw data should be
      """
      {"hello":{"world":"boo"}}
      """

  Scenario: Should be able delete from the reference and show up in the root
    Given I have a data instance with
      """
      {"hello":{"world":{"foo":"bar","bar":"foo"}}}
      """
      And I have reference p1 from data and property "hello"
     When I delete the p1 property "world.foo"
      And The raw data should be
      """
      {"hello":{"world":{"bar":"foo"}}}
      """
     Then I delete the p1 property "world.bar"
      And The raw data should be
      """
      {"hello":{"world":{}}}
      """

  Scenario: Should throw an error if the referencee is not an object
    Given I have a data instance with
      """
      {"hello":"world"}
      """
     Then I should get an error when creating a reference from data and property "hello"

  Scenario: Should return an empty object if reference doesnt exist
    Given I have an empty data instance
     When I have reference p1 from data and property "hello"
      And The raw data for p1 should be
      """
      {}
      """

  Scenario: Should return an empty object if reference doesnt exist with a default value
    Given I have an empty data instance
     Then I have reference p1 from data and property "hello" with
      """
      {"world":"foo"}
      """
      And The raw data for p1 should be
      """
      {"world":"foo"}
      """
      And The raw data should be
      """
      {"hello":{"world":"foo"}}
      """

  Scenario: A reference should have a parent reference
    Given I have a data instance with
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
     When I have reference p1 from data and property "hello"
      And Reference p1 should have data as a parent
      And Reference p1 should have data as root
     Then I have reference p2 from p1 and property "foo"
      And Reference p2 should have p1 as a parent
      And Reference p2 should have data as root

  Scenario: Should be able to reset the reference data and update the root
    Given I have a data instance with
      """
      {"hello":{"world":{"foo":"bar"}}}
      """
     When I have reference p1 from data and property "hello"
      And I reset the data for p1 to
      """
      {"foo":"bar"}
      """
      And The raw data for p1 should be
      """
      {"foo":"bar"}
      """
      And The raw data should be
      """
      {"hello":{"foo":"bar"}}
      """
     Then I reset the data for p1
      And The raw data for p1 should be
      """
      {}
      """
      And The raw data should be
      """
      {"hello":{}}
      """
