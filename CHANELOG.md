# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.6] - 2019-08-22
### Removed

- Removed some dev dependencies and build steps

## [1.0.5] - 2019-08-21
### Updated

- Added build scripts to the package and removed the dependency on the dev-tools.
- Added support for Yaml via the `toYaml` and `fromYaml` methods on the `Data` class.
- Amended the `Config` class to check the file extension to determine which to import, Yaml or Json.

## [1.0.4] - 2018-10-24
### Updated

- Amended package script to use `prepublishOnly` instead of `prepare`.

## [1.0.3] - 2018-10-22
### Updated

- Updated dev-tools depedency.
- Updated CircleCI config due to updated requirements.
- Updated README with updated shields.

## [1.0.2] - 2018-10-19
### Updated

- Updated the README with badges from the CircleCI builds.
- Minor adjustments to the CircleCI config to store the build badges.

## [1.0.1] - 2018-10-18
### Added

- Included the [unlicense] license details.
- Added this CHANGELOG file.
- Implemented submodule into the develop branch to handle the builds.
- Added to CircleCI.

## [1.0.0]

Initial rewrite.

[unlicense]: http://unlicense.org/
[1.0.0]: https://bitbucket.org/depax/depax-data/src/v1.0.0/
[1.0.1]: https://bitbucket.org/depax/depax-data/src/v1.0.1/
[1.0.2]: https://bitbucket.org/depax/depax-data/src/v1.0.2/
[1.0.3]: https://bitbucket.org/depax/depax-data/src/v1.0.3/
[1.0.4]: https://bitbucket.org/depax/depax-data/src/v1.0.4/
[1.0.5]: https://bitbucket.org/depax/depax-data/src/v1.0.5/
[1.0.6]: https://bitbucket.org/depax/depax-data/src/v1.0.6/
