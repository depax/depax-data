/**
 * Provides the Data Config class.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import { access, constants, readFile, writeFile } from "fs";
import { extname } from "path";
import { promisify } from "util";
import Data from "./Data";

const pAccess = promisify(access);
const pReadFile = promisify(readFile);
const pWriteFile = promisify(writeFile);

const sFilename = Symbol();

export default class Config extends Data {
    // #region Private properties.
    /** @todo */
    private [sFilename]: string;
    // #endregion

    /**
     * Construct the data access object, and provide any additional details.
     *
     * @param filename The filename of the config.
     * @param data The data object to manipulate, if null then one is created.
     * @param seperator The seperator to use to seperate each xpath element.
     */
    public constructor(filename: string, data: any = {}, seperator = ".") {
        super(data, seperator);
        this[sFilename] = filename;
    }

    // #region Public properties.
    /** Get and set the config filename. */
    public get filename(): string {
        return this[sFilename];
    }

    public set filename(filename: string) {
        this[sFilename] = filename;
    }
    // #endregion
    // #region Public methods.
    /**
     * Loads the config file.
     *
     * @emits loaded Emitted after the data has been loaded from a file.
     *
     * @throws {EMissingFilename} Thrown if the filename has not been provided or set.
     */
    public async load(): Promise<void> {
        await pAccess(this.filename, constants.R_OK);
        const data = await pReadFile(this.filename);

        if (extname(this.filename) === ".yaml") {
            this.fromYaml(data.toString());
        } else {
            this.fromJson(data.toString());
        }

        this.modified = false;
        this.emit("loaded", this);
    }

    /**
     * Saves the config file.
     *
     * @emits saved Emitted after the data has been saved to a file.
     *
     * @throws {EMissingFilename} Thrown if the filename has not been provided or set.
     */
    public async save(): Promise<void> {
        if (extname(this.filename) === ".yaml") {
            await pWriteFile(this.filename, this.toYaml());
        } else {
            await pWriteFile(this.filename, this.toJson());
        }

        this.modified = false;
        this.emit("saved", this);
    }
    // #endregion
}
