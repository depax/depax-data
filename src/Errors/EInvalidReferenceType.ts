/**
 * The error that is raised when trying to create a reference from an invalid
 * field type.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

export default class EInvalidReferenceType extends Error {
    /**
     * Create the error, and define the error values.
     *
     * @param field The name of the field causing the error.
     * @param type The value type of the field causing the error.
     */
    public constructor(readonly field: string, readonly type: string) {
        super(`The value "${field}" can not be used as a reference as it is of type "${type}", and must be an Object.`);

        this.name = this.constructor.name;
        this.stack = (new Error() as any).stack;
    }
}
