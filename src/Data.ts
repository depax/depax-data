/**
 * Provides the Data class allowing object manipulation.
 *
 * @author Nic Ashton <nic.ashton109@gmail.com>
 */

"use strict";

import * as EventEmitter from "eventemitter3";
import * as yaml from "yaml";
import EInvalidReferenceType from "./Errors/EInvalidReferenceType";

export interface IData {
    [name: string]: any;
}

export default class Data extends EventEmitter {
    // #region Protected properties.
    /** Determines if the data has been modified. */
    protected modified = false;
    // #endregion

    /**
     * Construct the data access object, and provide any additional details.
     *
     * @param data The data object to manipulate, if null then one is created.
     * @param xpathSeperator The seperator to use to seperate each xpath element.
     */
    public constructor(protected data: IData = {}, protected xpathSeperator = "." ) {
        super();
    }

    // #region Public properties.
    /** Returns the defined seperator character. */
    public get seperator(): string {
        return this.xpathSeperator;
    }

    /** Returns the raw data object. */
    public get getData(): any {
        return this.data;
    }

    /** Determines if the data has been modified. */
    public get isModified(): boolean {
        return this.modified;
    }
    // #endregion
    // #region Public methods.
    /**
     * Resets the internal data references to the provided data object.
     *
     * @param data The data object to reset the internal reference.
     *
     * @returns Returns self.
     *
     * @emits reset Emitted with this.
     */
    public reset(data: IData = {}): Data {
        this.data = data;
        this.modified = true;

        this.emit("reset", this);
        return this;
    }

    /**
     * Returns the data as a JSON.
     *
     * @param pretty Output the JSON in a pretty view.
     *
     * @returns Returns the JSON output.
     */
    public toJson(pretty = true): string {
        return pretty ? JSON.stringify(this.data, null, 4) : JSON.stringify(this.data);
    }

    /**
     * Returns the data as a YAML.
     *
     * @returns Returns the YAML output.
     */
    public toYaml(): string {
        return yaml.stringify(this.data);
    }

    /**
     * Imports and sets the data from a JSON string.
     *
     * @param data The JSON string to parse into the data object.
     *
     * @returns Returns self.
     */
    public fromJson(data: string): Data {
        return this.reset(JSON.parse(data));
    }

    /**
     * Imports and sets the data from a YAML string.
     *
     * @param data The YAML string to parse into the data object.
     *
     * @returns Returns self.
     */
    public fromYaml(data: string): Data {
        return this.reset(yaml.parse(data));
    }

    /**
     * Determines if the provided property exists.
     *
     * @param name The name of the property to check.
     *
     * @returns Returns true or false.
     */
    public has(name: string): boolean {
        /* tslint:disable-next-line:no-eval */
        return eval(`try{this.data["${this.nameToXPath(name)}"]!==undefined;}catch(e){false;}`);
    }

    /**
     * Get the value of the property, or the default value.
     *
     * @param name The name of the property to get.
     * @param def The default value to return if the property doesnt exist.
     *
     * @returns Returns the property value, otherwise the def value.
     */
    public get(name: string, def: any = null): any {
        const n = this.nameToXPath(name);
        if (arguments.length === 1) {
            arguments[1] = def;
        }

        /* tslint:disable-next-line:no-eval */
        return eval(`try{this.data["${n}"]!==undefined?this.data["${n}"]:arguments[1];}catch(err){arguments[1];}`);
    }

    /**
     * Set the value of a property.
     *
     * @param name The name of the property to set.
     * @param value The value to assign, if the value is null then the field is deleted.
     *
     * @returns Returns self.
     *
     * @emits update Emitted with this, name and value.
     */
    public set(name: string, value: any): Data {
        const ns = name.split(this.seperator);
        let xpath = "";

        for (let i = 0, len = ns.length - 1; i < len; i++) {
            xpath += (i > 0 ? '"]["' : "") + ns[i];

            /* tslint:disable-next-line:no-eval */
            eval(`if(this.data["${xpath}"]===undefined){this.data["${xpath}"]={};}`);
        }

        this.modified = true;

        /* tslint:disable-next-line:no-eval */
        eval(`this.data["${this.nameToXPath(name)}"]=arguments[1];`);

        this.emit("update", this, name, value);

        return this;
    }

    /**
     * Deletes the value of a property.
     *
     * @param name The name of the property to delete.
     *
     * @returns Returns self.
     *
     * @emits update Emitted with this, and name.
     */
    public del(name: string): Data {
        this.modified = true;

        try {
            /* tslint:disable-next-line:no-eval */
            eval(`delete this.data["${this.nameToXPath(name)}"];`);

            this.emit("update", this, name);
        } catch (err) {
            // Do nothing.
        }

        return this;
    }

    /**
     * Generate a reference to the specified xpath, this only works if the value
     * is an object. If the value doesnt exist, one is created as an empty
     * object.
     *
     * @param name The name of the property to reference.
     * @param def The default value of the object property.
     *
     * @returns The resulting reference.
     *
     * @throws {EInvalidReferenceType} Thrown if the value is anything but an object.
     */
    public ref(name: string, def?: any): DataReference {
        if (this.has(name) === false) {
            this.set(name, def && typeof def === "object" ? def : {});
        }

        const val = this.get(name);
        if (typeof val !== "object") {
            throw new EInvalidReferenceType(name, typeof val);
        }

        return new DataReference(this, name, val, this.seperator);
    }
    // #endregion
    // #region Protected methods.
    /**
     * Helper function to convert a property name to an XPath used through eval
     * to get or set a config value.
     *
     * @param name The name of the property.
     *
     * @returns The name converted to an eval xpath.
     */
    protected nameToXPath(name: string): string {
        return name.split(this.seperator).join('"]["');
    }
    // #endregion
}

/* tslint:disable:max-classes-per-file */
export class DataReference extends Data {
    /**
     * Construct the data reference object.
     *
     * @param parent A parent Data object if this is a reference of an item from the parent.
     * @param xpath The xpath of the reference within the parent.
     * @param data The data object to manipulate, if null then one is created.
     * @param seperator The seperator to use to seperate each xpath element.
     */
    public constructor(protected parent: Data, protected xpath: string, data: any, seperator: string) {
        super(data, seperator);
    }

    // #region Public properties.
    /** Get the parent reference. */
    public get getParent(): Data {
        return this.parent;
    }

    /** Get the root data element. */
    public get getRoot(): Data {
        let p = this.parent;
        while (p instanceof DataReference) {
            p = p.getParent;
        }

        return p;
    }
    // #endregion
    // #region Public methods.
    /** @inheritdoc */
    public reset(data: any = {}): Data {
        super.reset(data);
        this.parent.set(this.xpath, this.data);

        return this;
    }

    /** @inheritdoc */
    public set(name: string, value: any): Data {
        super.set(name, value);
        this.parent.set(this.xpath, this.data);

        return this;
    }

    /** @inheritdoc */
    public del(name: string): Data {
        super.del(name);
        this.parent.set(this.xpath, this.data);

        return this;
    }
    // #nedregion
}
